import 'package:bpjs/berita.dart';
import 'package:bpjs/kontak.dart';
import 'package:bpjs/new_user.dart';
import 'package:bpjs/notifikasi.dart';
import 'package:bpjs/profil.dart';
import 'package:flutter/material.dart';

class MenuLayanan extends StatefulWidget {
  const MenuLayanan({Key? key}) : super(key: key);

  @override
  _MenuLayananState createState() => _MenuLayananState();
}

class _MenuLayananState extends State<MenuLayanan> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("BPJSTKU"),
        centerTitle: true,
        backgroundColor: Colors.blue,
      ),
      body: Column(
        children: [
          Container(
            color: Colors.blue,
            width: double.infinity,
            height: 40,
            child: Padding(
              padding: const EdgeInsets.only(top: 10, left: 80),
              child: Text("JAMINAN HARI TUA (JHT)",
                style: TextStyle(fontSize: 20,
                    color: Colors.white.withOpacity(0.9)),),
            ),
          ),
          Container(
            color: Colors.blue,
          height: 150,
          child: Row(
            children: [
              Container(
                child: Image.asset('assets/images/icon_saldo.png'),
                margin: EdgeInsets.only(left: 20),
                height: 100,
                width: 100,
              ),
              Container(
                child: Image.asset('assets/images/icon_simulation.png'),
                margin: EdgeInsets.only(left: 20),
                height: 100,
                width: 100,
              ),
              Container(
                child: Image.asset('assets/images/icon_queue.png'),
                margin: EdgeInsets.only(left: 20),
                height: 100,
                width: 100,
              ),
            ],
          ),
          ),
          Row(
            children: [
              Container(
                child: Image.asset('assets/images/icon_card.png'),
                margin: EdgeInsets.only(left: 20, top: 20),
                height: 100,
                width: 100,
              ),
              Container(
                child: Image.asset('assets/images/icon_report.png'),
                margin: EdgeInsets.only(left: 20, top: 20),
                height: 100,
                width: 100,
              ),
              Container(
                child: Image.asset('assets/images/icon_info.png'),
                margin: EdgeInsets.only(left: 20, top: 20),
                height: 100,
                width: 100,
              ),
            ],
          ),
          Row(
            children: [
              Container(
                child: Text("Kartu Digital", style: TextStyle(
                    color: Colors.black),),
                margin: EdgeInsets.only(left: 20, top: 10),
                width: 100,
              ),
              Container(
                child: Text("Kecelakaan Kerja", style: TextStyle(
                    color: Colors.black),),
                margin: EdgeInsets.only(left: 20, top: 10),
              ),
              Container(
                child: Text("Info Program", style: TextStyle(
                    color: Colors.black),),
                margin: EdgeInsets.only(left: 20, top: 10),
                width: 100,
              ),
            ],
          ),
          Row(
            children: [
              Container(
                child: Image.asset('assets/images/icon_partner.png'),
                margin: EdgeInsets.only(left: 20, top: 20),
                height: 100,
                width: 100,
              ),
              Container(
                child: Image.asset('assets/images/icon_branch.png'),
                margin: EdgeInsets.only(left: 20,top: 20),
                height: 100,
                width: 100,
              ),
              Container(
                child: Image.asset('assets/images/icon_complaint.png'),
                margin: EdgeInsets.only(left: 20, top: 20),
                height: 100,
                width: 100,
              ),
            ],
          ),
          Row(
            children: [
              Container(
                child: Text("Mitra Layanan", style: TextStyle(
                    color: Colors.black),),
                margin: EdgeInsets.only(left: 20, top: 10),
                width: 100,
              ),
              Container(
                child: Text("Kantor Cabang", style: TextStyle(
                    color: Colors.black),),
                margin: EdgeInsets.only(left: 20, top: 10),
                width: 100,
              ),
              Container(
                child: Text("Pengaduan", style: TextStyle(
                    color: Colors.black),),
                margin: EdgeInsets.only(left: 20, top: 10),
                width: 100,
              ),
            ],
          ),
        ],
      ),
   /*   bottomNavigationBar: BottomNavigationBar(
        items: [
          BottomNavigationBarItem(icon: Icon(Icons.work),label: 'Layanan'),
          BottomNavigationBarItem(icon: Icon(Icons.inbox),label: 'Notifikasi'),
          BottomNavigationBarItem(icon: Icon(Icons.info),label: 'Berita'),
          BottomNavigationBarItem(icon: Icon(Icons.headset_mic), label: 'Kontak'),
          BottomNavigationBarItem(icon: Icon(Icons.person), label: 'Profil'),
        ],
        type: BottomNavigationBarType.fixed,
        selectedItemColor: Colors.grey,
      ), */
    );
  }
}
