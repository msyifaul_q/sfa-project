import 'package:flutter/material.dart';

class MenuNotifikasi extends StatefulWidget {
  const MenuNotifikasi({Key? key}) : super(key: key);

  @override
  _MenuNotifikasiState createState() => _MenuNotifikasiState();
}

class _MenuNotifikasiState extends State<MenuNotifikasi> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Notifikasi"),
        centerTitle: true,
        backgroundColor: Colors.blue,
      ),
      body: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 200, left: 180),
              child: Icon(
                Icons.hourglass_empty_outlined,
                color: Colors.grey,
                size: 50.0,
              ),
            ),
            Container(
              child: Text("Belum Ada Notifikasi"),
              margin: EdgeInsets.only(left: 180),
            ),
          ],
        ),
    );
  }
}



