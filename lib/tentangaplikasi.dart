import 'package:flutter/material.dart';

class MenuTentangAplikasi extends StatefulWidget {
  const MenuTentangAplikasi({Key? key}) : super(key: key);

  @override
  _MenuTentangAplikasiState createState() => _MenuTentangAplikasiState();
}

class _MenuTentangAplikasiState extends State<MenuTentangAplikasi> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: BackButton(
            color: Colors.white
        ),
        title: Text("Tentang Aplikasi"),
      ),
      body: Container(
              child: Text("Aplikasi BPJSTKU By : Ahsanul Fatikhin, Syifaul Qulub, Nurul Hidayat @2021"),
              margin: EdgeInsets.only(left: 120, top: 200),
              width: 250,
            ),
    );
  }
}
