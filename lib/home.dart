import 'package:bpjs/login.dart';
import 'package:bpjs/new_member.dart';
import 'package:bpjs/new_user.dart';
import 'package:flutter/material.dart';

class MenuHome extends StatefulWidget {
  const MenuHome({Key? key}) : super(key: key);

  @override
  _MenuHomeState createState() => _MenuHomeState();
}

class _MenuHomeState extends State<MenuHome> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Column(
        children: [
          Container(
            child: Image.asset('assets/images/home.jpeg'),
            //  height: 450,
          ),
          Row(
            children: [
              GestureDetector(
                onTap: () {
                  // print("Saya telah klik RaisedButton");
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => MenuNewMember()),
                  );
                },
                child: Container(
                  child: Image.asset('assets/images/newmember.jpeg'),
                  margin: EdgeInsets.only(left: 80, top: 20),
                  height: 100,
                  width: 100,
                ),
              ),
              GestureDetector(
                onTap: () {
                  // print("Saya telah klik RaisedButton");
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => MenuNewUser()),
                  );
                },
                child: Container(
                  child: Image.asset('assets/images/icon_new_user.jpeg'),
                  margin: EdgeInsets.only(left: 80, top: 20),
                  height: 100,
                  width: 100,
                ),
              ),
            ],
          ),
          Row(
            children: [
              Container(
                margin: EdgeInsets.only(left: 50, top: 10),
                child: Text(
                  "PENDAFTARAN PESERTA BARU",
                  style: TextStyle(
                      fontSize: 10, color: Colors.black.withOpacity(0.9)),
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 30, top: 10),
                child: Text(
                  "PENDAFTARAN PENGGUNA BARU",
                  style: TextStyle(
                      fontSize: 10, color: Colors.black.withOpacity(0.9)),
                ),
              ),
            ],
          ),
          Container(
            width: 400,
            child: RaisedButton(
              color: Colors.blue,
              child: Text(
                "Login",
                style: TextStyle(color: Colors.white.withOpacity(0.9)),
              ),
              elevation: 10,
              // Cara untuk disable
              // onLongPress: null,
              onPressed: () {
                // print("Saya telah klik RaisedButton");
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => MenuLogin()),
                );
              },
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(100)),
            ),
          ),
        ],
      ),
    );
  }
}
