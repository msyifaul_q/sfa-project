import 'package:bpjs/berita.dart';
import 'package:bpjs/kontak.dart';
import 'package:bpjs/layanan.dart';
import 'package:bpjs/notifikasi.dart';
import 'package:bpjs/profil.dart';
import 'package:flutter/material.dart';

class MenuBottomNavigation extends StatefulWidget {
  const MenuBottomNavigation({Key? key}) : super(key: key);

  @override
  _MenuBottomNavigationState createState() => _MenuBottomNavigationState();
}

class _MenuBottomNavigationState extends State<MenuBottomNavigation> {
  List<Widget> listScreen = [
    MenuLayanan(),
    MenuNotifikasi(),
    MenuBerita(),
    MenuKontak(),
    MenuProfil()
  ];
  int currentScreen = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: listScreen.elementAt(currentScreen),
      bottomNavigationBar: BottomNavigationBar(
        items: [
            BottomNavigationBarItem(icon: Icon(Icons.work),label: 'Layanan'),
            BottomNavigationBarItem(icon: Icon(Icons.inbox),label: 'Notifikasi'),
            BottomNavigationBarItem(icon: Icon(Icons.info),label: 'Berita'),
            BottomNavigationBarItem(icon: Icon(Icons.headset_mic), label: 'Kontak'),
            BottomNavigationBarItem(icon: Icon(Icons.person), label: 'Profil'),
        ],
        type: BottomNavigationBarType.fixed,
        currentIndex: this.currentScreen,
        selectedItemColor: Colors.blue,
        onTap: (int index){
          setState(() {
            this.currentScreen = index;
          });
        },
      ),
    );
  }
}
