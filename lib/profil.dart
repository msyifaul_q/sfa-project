import 'package:bpjs/home.dart';
import 'package:bpjs/syaratketentuan.dart';
import 'package:bpjs/tentangaplikasi.dart';
import 'package:flutter/material.dart';

class MenuProfil extends StatefulWidget {
  const MenuProfil({Key? key}) : super(key: key);

  @override
  _MenuProfilState createState() => _MenuProfilState();
}

class _MenuProfilState extends State<MenuProfil> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Akun Saya"),
        centerTitle: true,
        backgroundColor: Colors.blue,
      ),
      body: ListView(
        children: [
          Card(
            child: ListTile(
              leading: CircleAvatar(
                backgroundImage: AssetImage('assets/images/home.jpeg'),
              ),
              title: Padding(
                padding: const EdgeInsets.only(left: 20),
                child: Text(
                  "Soekarno",
                  style: TextStyle(fontSize: 30),
                ),
              ),
              subtitle: Padding(
                padding: const EdgeInsets.only(left: 20),
                child: Text("admin@soekarno.id"),
              ),
              trailing: RaisedButton(
                child: Text(
                  "Pengaturan",
                  style: TextStyle(color: Colors.white),
                ),
                color: Colors.blue,
                onPressed: () => print('Pengaturan akun'),
              ),
              contentPadding:
                  EdgeInsets.symmetric(vertical: 20.0, horizontal: 16.0),
            ),
          ),
          Card(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  child: Text(
                    "Profil Saya",
                    style: TextStyle(color: Colors.black, fontSize: 20),
                  ),
                  margin: EdgeInsets.only(left: 20, top: 10),
                ),
                Container(
                  child: Text(
                    "Nama Lengkap",
                    style: TextStyle(color: Colors.grey, fontSize: 10),
                  ),
                  margin: EdgeInsets.only(left: 20, top: 10),
                ),
                Container(
                  child: Text(
                    "Soekarno",
                    style: TextStyle(color: Colors.black, fontSize: 15),
                  ),
                  margin: EdgeInsets.only(left: 20),
                ),
                Container(
                  child: Text(
                    "Nomor KTP/Paspor",
                    style: TextStyle(color: Colors.grey, fontSize: 10),
                  ),
                  margin: EdgeInsets.only(left: 20, top: 10),
                ),
                Container(
                  child: Text(
                    "353232435342323",
                    style: TextStyle(color: Colors.black, fontSize: 15),
                  ),
                  margin: EdgeInsets.only(left: 20),
                ),
                Container(
                  child: Text(
                    "Tanggal Lahir",
                    style: TextStyle(color: Colors.grey, fontSize: 10),
                  ),
                  margin: EdgeInsets.only(left: 20, top: 10),
                ),
                Container(
                  child: Text(
                    "Surabaya, 06 Juni 1901",
                    style: TextStyle(color: Colors.black, fontSize: 15),
                  ),
                  margin: EdgeInsets.only(left: 20),
                ),
                Container(
                  child: Text(
                    "Alamat",
                    style: TextStyle(color: Colors.grey, fontSize: 10),
                  ),
                  margin: EdgeInsets.only(left: 20, top: 10),
                ),
                Container(
                  child: Text(
                    "Surabaya",
                    style: TextStyle(color: Colors.black, fontSize: 15),
                  ),
                  margin: EdgeInsets.only(left: 20, bottom: 20),
                ),
              ],
            ),
          ),
          Card(
            child: Column(
              children: [
                ListTile(
                  leading: Icon(Icons.phonelink_setup),
                  title: Text("Tentang Aplikasi"),
                  trailing: Icon(Icons.arrow_forward_ios_rounded),
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => MenuTentangAplikasi()),
                    );
                  },
                ),
                ListTile(
                  leading: Icon(Icons.lock_rounded),
                  title: Text("Syarat & Ketentuan"),
                  trailing: Icon(Icons.arrow_forward_ios_rounded),
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => MenuSyaratKetentuan()),
                    );
                  },
                ),
                ListTile(
                  leading: Icon(Icons.logout),
                  title: Text("Logout"),
                  trailing: Icon(Icons.arrow_forward_ios_rounded),
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => MenuHome()),
                    );
                  },
                ),
              ],
            ),
          ),
        ],
        padding: EdgeInsets.all(10),
        shrinkWrap: true,
      ),
    );
  }
}
