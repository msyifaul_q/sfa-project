import 'package:flutter/material.dart';

class MenuBerita extends StatefulWidget {
  const MenuBerita({Key? key}) : super(key: key);

  @override
  _MenuBeritaState createState() => _MenuBeritaState();
}

class _MenuBeritaState extends State<MenuBerita> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Berita"),
        centerTitle: true,
        backgroundColor: Colors.blue,
      ),
      body: ListView(
        children: [
          Card(
              child: ListTile(
                leading: Container(
                  child:
                  Image.asset('assets/images/home.jpeg',
                    height: 100,
                    width: 100,
                    fit: BoxFit.fill,),
                    height: 100,
                  color: Colors.grey,
                ),
                title:  Text("BP Jamsostek Madura bagi-bagi masker dan multivitamin"),
                subtitle: Padding(
                  padding: const EdgeInsets.only(top: 20),
                  child: Text("4 Hari yang lalu"),
                ),
                contentPadding: EdgeInsets.symmetric(vertical: 40.0,
                    horizontal: 16.0),
              ),
          ),
          Card(
            child: ListTile(
              leading: Container(
                child: Image.asset('assets/images/home.jpeg',
                  height: 300,
                  width: 100,
                  fit: BoxFit.fitWidth,),
                height: 300,
                color: Colors.grey,
              ),
              title:  Text("BP Jamsostek Madura bagi-bagi masker dan multivitamin"),
              subtitle: Padding(
                padding: const EdgeInsets.only(top: 20),
                child: Text("4 Hari yang lalu"),
              ),
              contentPadding: EdgeInsets.symmetric(vertical: 40.0,
                  horizontal: 16.0),
            ),
          ),
          Card(
            child: ListTile(
              leading: Container(
                child: Image.asset('assets/images/home.jpeg',
                  height: 300,
                  width: 100,
                  fit: BoxFit.fitWidth,),
                height: 300,
                color: Colors.grey,
              ),
              title:  Text("BP Jamsostek Madura bagi-bagi masker dan multivitamin"),
              subtitle: Padding(
                padding: const EdgeInsets.only(top: 20),
                child: Text("4 Hari yang lalu"),
              ),
              contentPadding: EdgeInsets.symmetric(vertical: 40.0,
                  horizontal: 16.0),
            ),
          ),
          Card(
            child: ListTile(
              leading: Container(
                child: Image.asset('assets/images/home.jpeg',
                  height: 300,
                  width: 100,
                  fit: BoxFit.fitWidth,),
                height: 300,
                color: Colors.grey,
              ),
              title:  Text("BP Jamsostek Madura bagi-bagi masker dan multivitamin"),
              subtitle: Padding(
                padding: const EdgeInsets.only(top: 20),
                child: Text("4 Hari yang lalu"),
              ),
              contentPadding: EdgeInsets.symmetric(vertical: 40.0,
                  horizontal: 16.0),
            ),
          ),
        ],
        padding: EdgeInsets.all(10),
        shrinkWrap: true,
      ),
    );
  }
}
