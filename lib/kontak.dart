import 'package:flutter/material.dart';

class MenuKontak extends StatefulWidget {
  const MenuKontak({Key? key}) : super(key: key);

  @override
  _MenuKontakState createState() => _MenuKontakState();
}

class _MenuKontakState extends State<MenuKontak> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Kontak"),
        centerTitle: true,
        backgroundColor: Colors.blue,
      ),
      body: SingleChildScrollView(
        child: Column(
        children: [
          Container(
            child: Text("Layanan Masyarakat", style: TextStyle(
              fontSize: 20, color: Colors.black
            ),),
            margin: EdgeInsets.only(),
          ),
          Container(
            child: Image.asset('assets/images/img_ask_bpjs.png'),
            margin: EdgeInsets.only(top: 10),
            height: 100,
            width: 100,
          ),
         Row(
              children: [
                Container(
                  child: Image.asset('assets/images/icon_whatsapp.png'),
                  margin: EdgeInsets.only(top: 10, left: 170),
                  height: 50,
                  width: 50,
                ),
                Container(
                  child: Text("Whatsapp", style: TextStyle(color: Colors.grey),),
                  margin: EdgeInsets.only(left: 10),
                )
              ],
          ),
              Container(
                child: Text("+62 811 9115910", style: TextStyle(color: Colors.black),),
                margin: EdgeInsets.only(top: 20),
              ),
              Container(
                child: Text("+62 855 1500910", style: TextStyle(color: Colors.black),),
                margin: EdgeInsets.only(),
              ),
              Container(
                child: Text("Layanan Whatsapp hanya untuk pekerja Migran Indonesia di Luar Negri ",
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.green),),
                margin: EdgeInsets.only(top: 20),
                width: 250,
              ),
              Container(
                child: Text("Layanan Masyarakat BPJS Ketenagakerjaan Melayani Andai mulai pukul 06:00 hingga pukul 22:00 WIB ",
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.black),),
                margin: EdgeInsets.only(top: 20),
                width: 300,
              ),
              Container(
                child: Image.asset('assets/images/img_customer_care.png'),
                margin: EdgeInsets.only(top: 20),
                height: 300,
                width: 300,
              ),
        ],
      ),
      ),
    );
  }
}
