import 'package:bpjs/berita.dart';
import 'package:bpjs/kontak.dart';
import 'package:bpjs/layanan.dart';
import 'package:bpjs/login.dart';
import 'package:bpjs/new_member.dart';
import 'package:bpjs/new_user.dart';
import 'package:bpjs/notifikasi.dart';
import 'package:bpjs/profil.dart';
import 'package:bpjs/bottomnavigation.dart';
import 'package:bpjs/syaratketentuan.dart';
import 'package:bpjs/tentangaplikasi.dart';
import 'package:flutter/material.dart';

import 'home.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    {
      return MaterialApp(
          title: 'Flutter Demo',
          theme: ThemeData(
            primarySwatch: Colors.blue,
          ),
          home: MenuHome()
      );
    }
  }
}

