import 'package:flutter/material.dart';

class MenuNewMember extends StatefulWidget {
  const MenuNewMember({Key? key}) : super(key: key);

  @override
  _MenuNewMemberState createState() => _MenuNewMemberState();
}

class _MenuNewMemberState extends State<MenuNewMember> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: BackButton(
            color: Colors.white
        ),
        title: Text("Peserta Baru"),
      ),
      body: Container(
        child: Text("Soon..."),
        margin: EdgeInsets.only(left: 250, top: 250),
      ),
    );
  }
}
