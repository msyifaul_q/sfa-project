import 'package:bpjs/bottomnavigation.dart';
import 'package:bpjs/home.dart';
import 'package:bpjs/layanan.dart';
import 'package:flutter/material.dart';

class MenuLogin extends StatefulWidget {
  const MenuLogin({Key? key}) : super(key: key);

  @override
  _MenuLoginState createState() => _MenuLoginState();
}

class _MenuLoginState extends State<MenuLogin> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
            child: Image.asset('assets/images/home.jpeg'),
            //  height: 450,
          ),
          Container(
              margin: EdgeInsets.only(top: 10),
              child: Text(
                "Masuk Ke Akun Saya",
                style: TextStyle(fontSize: 15, color: Colors.black),
              )
              //  height: 450,
              ),
          Container(
            width: 400,
            child: TextFormField(
              decoration: InputDecoration(
                labelText: "NAMA PENGGUNA",
              ),
            ),
          ),
          Container(
            width: 400,
            child: TextFormField(
              decoration: InputDecoration(
                labelText: "KATA SANDI",
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 20),
            width: 400,
            child: RaisedButton(
              color: Colors.blue,
              child: Text(
                "Login",
                style: TextStyle(color: Colors.white.withOpacity(0.9)),
              ),
              elevation: 10,
              // Cara untuk disable
              // onLongPress: null,
              onPressed: () {
                // print("Saya telah klik RaisedButton");
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => MenuBottomNavigation()),
                );
              },
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(100)),
            ),
          ),
          Row(
            children: [
              Container(
                margin: EdgeInsets.only(left: 60, top: 10),
                child: InkWell(
                  child: Text("LUPA KATA SANDI", style: TextStyle(
                      color: Colors.blue),),
                  onTap: () {
                  //  print("Saya telah klik InkWell");
                  },
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 80, top: 10),
                child: InkWell(
                  child: Text("DAFTAR AKUN", style: TextStyle(
                      color: Colors.blue),),
                  onTap: () {
                    //  print("Saya telah klik InkWell");
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => MenuHome()),
                    );
                  },
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
