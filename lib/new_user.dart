import 'package:flutter/material.dart';

class MenuNewUser extends StatefulWidget {
  const MenuNewUser({Key? key}) : super(key: key);

  @override
  _MenuNewUserState createState() => _MenuNewUserState();
}

class _MenuNewUserState extends State<MenuNewUser> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: BackButton(
            color: Colors.white
        ),
        title: Text("Pengguna Baru"),
      ),
      body: Container(
        child: Text("Soon..."),
        margin: EdgeInsets.only(left: 250, top: 250),
      ),
    );
  }
}
